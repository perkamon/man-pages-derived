# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"#-#-#-#-#  man-pages.pot (PACKAGE VERSION)  #-#-#-#-#\n"
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-01-26 01:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"
"#-#-#-#-#  - (PACKAGE VERSION)  #-#-#-#-#\n"
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-01-26 01:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"
"#-#-#-#-#  man-pages-dist-debian.pot (PACKAGE VERSION)  #-#-#-#-#\n"
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-02-05 23:17+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING"

#. type: TH
#: build/C/man2/adjtime.2:23
#, no-wrap
msgid "2002-02-16"
msgstr ""

#. type: TH
#: build/C/man2/adjtime.2:23
#, no-wrap
msgid "Linux 2.0"
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:26
msgid "adjtime - smoothly tune kernel clock"
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:30
msgid ""
"B<int adjtime(const struct timeval *>I<delta>B<, struct timeval "
"*>I<olddelta>B<);>"
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:35
msgid ""
"This function speeds up or slows down the system clock in order to make a "
"gradual adjustment.  This ensures that the calendar time reported by the "
"system clock is always monotonically increasing, which might not happen if "
"you simply set the clock."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:42
msgid ""
"The I<delta> argument specifies a relative adjustment to be made to the "
"clock time.  If negative, the system clock is slowed down fora while until "
"it has lost this much elapsed time.  If positive, the system clock is "
"speeded up for a while."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:49
msgid ""
"If the I<olddelta> argument is not a null pointer, the B<adjtime> function "
"returns information about any previous time adjustment that has not yet "
"completed."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:52
msgid ""
"This function is typically used to synchronize the clocks of computers in a "
"local network.  You must be a privileged user to use it."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:59
msgid ""
"The B<adjtime> function returns 0 on success and -1 on failure and sets the "
"external variable I<errno> accordingly."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:63
msgid ""
"The calling process does not have enough privileges to access the clock."
msgstr ""

#. type: SH
#: build/C/man2/adjtime.2:63
#, no-wrap
msgid "NOTE"
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:67
msgid ""
"With a Linux kernel, you can use the B<adjtimex>(2)  function to permanently "
"change the system clock speed."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:69
msgid "This function is derived from 4.3 BSD and SVr4."
msgstr ""

#. type: Plain text
#: build/C/man2/adjtime.2:71
msgid "B<adjtimex>(2), B<settimeofday>(2)"
msgstr ""

#. type: Plain text
#: build/C/man4/ttys.4:27
msgid "ttys - serial terminal lines"
msgstr ""

#. type: Plain text
#: build/C/man4/ttys.4:34
msgid "mknod -m 660 /dev/ttyS0 c 4 64 # base address 0x03f8"
msgstr ""

#. type: Plain text
#: build/C/man4/ttys.4:36
msgid "mknod -m 660 /dev/ttyS1 c 4 65 # base address 0x02f8"
msgstr ""

#. type: Plain text
#: build/C/man4/ttys.4:38
msgid "mknod -m 660 /dev/ttyS2 c 4 66 # base address 0x03e8"
msgstr ""

#. type: Plain text
#: build/C/man4/ttys.4:40
msgid "mknod -m 660 /dev/ttyS3 c 4 67 # base address 0x02e8"
msgstr ""

#. type: Plain text
#: build/C/man4/ttys.4:50
msgid "B<mknod>(1), B<chown>(1), B<getty>(1), B<tty>(4)"
msgstr ""

#. type: TH
#: build/C/man5/mailname.5:25
#, no-wrap
msgid "MAILNAME"
msgstr ""

#. type: TH
#: build/C/man5/mailname.5:25
#, no-wrap
msgid "2002-03-29"
msgstr ""

#. type: TH
#: build/C/man5/mailname.5:25 build/C/man7/missing.7:16
#: build/C/man7/undocumented.7:27
#, no-wrap
msgid "Debian GNU/Linux"
msgstr ""

#. type: Plain text
#: build/C/man5/mailname.5:28
msgid "mailname - the visible mail name of the system"
msgstr ""

#. type: Plain text
#: build/C/man5/mailname.5:34
msgid ""
"The file I</etc/mailname> is a plain ASCII configuration file, which on a "
"Debian system contains the visible mail name of the system.  It is used by "
"many different programs, usually programs that wish to send or relay mail, "
"and need to know the name of the system."
msgstr ""

#. type: Plain text
#: build/C/man5/mailname.5:38
msgid ""
"The file contains only one line describing the fully qualified domain name "
"that the program wishing to get the mail name should use (that is, "
"everything after the @)."
msgstr ""

#. type: Plain text
#: build/C/man5/mailname.5:40
msgid "B<mailaddr>(7), B<sendmail>(8)"
msgstr ""

#. type: TH
#: build/C/man5/motd.tail.5:23
#, no-wrap
msgid "MOTD.TAIL"
msgstr ""

#. type: TH
#: build/C/man5/motd.tail.5:23
#, no-wrap
msgid "2007-04-28"
msgstr ""

#. type: TH
#: build/C/man5/motd.tail.5:23
#, no-wrap
msgid "Debian"
msgstr ""

#. type: TH
#: build/C/man5/motd.tail.5:23
#, no-wrap
msgid "Debian Administrator's Manual"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:26
msgid "motd.tail - Template for building the system message of the day"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:31
msgid ""
"On Debian systems, the system message of the day is rebuilt at each startup, "
"in order to display an accurate information. I</etc/motd.tail> is the file "
"to edit permanent changes to the message of the day."
msgstr ""

#. type: SH
#: build/C/man5/motd.tail.5:31
#, no-wrap
msgid "OVERVIEW"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:41
msgid ""
"The initiation script I</etc/init.d/bootmisc.sh> prepends a line containing "
"information about the system to I</etc/motd.tail> and stores the resulting "
"file in I</var/run/motd>. I</etc/motd> is a symbolic link to I</var/run/"
"motd>.  This is done to prevent changes to I</etc> as the system can not "
"assume I</etc> to be writable."
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:46
msgid ""
"Changes to I</etc/motd> effectively end up in a file under I</var/run> which "
"will be regenerated upon reboot."
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:50
msgid ""
"A symbolic link to a different file, such as I</etc/motd.static> disables "
"this behaviour."
msgstr ""

#. type: TP
#: build/C/man5/motd.tail.5:51
#, no-wrap
msgid "I</etc/init.d/bootmisc.sh>"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:54
msgid "The initiation script which builds /var/run/motd"
msgstr ""

#. type: TP
#: build/C/man5/motd.tail.5:54
#, no-wrap
msgid "I</etc/motd>"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:57
msgid "Symbolic link to the system message of the day at /var/run/motd"
msgstr ""

#. type: TP
#: build/C/man5/motd.tail.5:57
#, no-wrap
msgid "I</etc/motd.tail>"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:60
msgid "Template for building the system message of the day"
msgstr ""

#. type: TP
#: build/C/man5/motd.tail.5:60
#, no-wrap
msgid "I</var/run/motd>"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:63
msgid "System message of the day file rebuilt at each computer start"
msgstr ""

#. type: Plain text
#: build/C/man5/motd.tail.5:67
msgid "B<login>(1), B<issue>(5), B<motd>(5)."
msgstr ""

#. type: TH
#: build/C/man7/LDP.7:19
#, no-wrap
msgid "LDP"
msgstr ""

#. type: TH
#: build/C/man7/LDP.7:19
#, no-wrap
msgid "2001-11-15"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:22
msgid ""
"LDP - Intro to the Linux Documentation Project, with help, guides and "
"documents"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:26
msgid ""
"The Linux Documentation Project (LDP) provides a variety of free "
"documentation resources including guides, FAQs, HOWTOs, and man-pages to the "
"Linux community."
msgstr ""

#. type: SH
#: build/C/man7/LDP.7:26
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:32
msgid ""
"The various documents in the LDP archives are maintained by individual "
"authors, and are listed in the beginning of each HOWTO.  If you have any "
"questions or inputs to a document we encourage you to contact the authors "
"directly."
msgstr ""

#. type: SH
#: build/C/man7/LDP.7:32
#, no-wrap
msgid "WEB PAGES"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:36
msgid ""
"The LDP has its own dedicated web site as do many of the various "
"translations projects which are linked from the main LDP web site at:"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:39
msgid "B<http://www\\&.tldp\\&.org/>"
msgstr ""

#. type: SH
#: build/C/man7/LDP.7:40
#, no-wrap
msgid "MAN PAGES"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:43
msgid ""
"A web page with status information for manual pages and translations is "
"found at:"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:46
msgid "B<http://www\\&.win\\&.tue\\&.nl/~aeb/linux/man/>"
msgstr ""

#. type: SH
#: build/C/man7/LDP.7:47
#, no-wrap
msgid "MAILING LISTS"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:49
msgid "LDP has a number of mailing lists, such as"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:51
#, no-wrap
msgid "B<E<lt>announce@en\\&.tlpd\\&.orgE<gt>>"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:55
msgid "Announcements from the LDP project"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:55
#, no-wrap
msgid "B<E<lt>discuss@en\\&.tldp\\&.orgE<gt>>"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:58
msgid "General discussion on the LDP project"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:58
#, no-wrap
msgid "B<E<lt>docbook@en\\&.tldp\\&.orgE<gt>>"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:61
msgid "Questions about the use of DocBook"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:63
msgid "For subscription information, see the website."
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:70
msgid ""
"If you are interested in DocBook beyond the simple markup of your LDP "
"document, you may want to consider joining one of the OASIS DocBook mailing "
"lists.  Please see B<http://docbook\\&.org/mailinglist/index\\&.html> for "
"more information."
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:72
msgid ""
"Most distributions include the HOWTOs and mini-HOWTOs in the installation"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:73
#, no-wrap
msgid "I</usr/doc/>                   (old place for documentation)"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:75
#, no-wrap
msgid "I</usr/share/doc/>             (new place for documentation)"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:77
#, no-wrap
msgid "I</usr/share/doc/HOWTO/>       (HOWTO files)"
msgstr ""

#. type: TP
#: build/C/man7/LDP.7:79
#, no-wrap
msgid "I</usr/share/doc/HOWTO/mini/>  (mini-HOWTO files)"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:86
msgid "B<info>(1), B<man>(1), B<xman>(1x)"
msgstr ""

#. type: Plain text
#: build/C/man7/LDP.7:90
msgid "B<info> pages as read with B<emacs>(1)  or B<info>(1)"
msgstr ""

#. type: TH
#: build/C/man7/missing.7:16
#, no-wrap
msgid "MISSING"
msgstr ""

#. type: TH
#: build/C/man7/missing.7:16
#, no-wrap
msgid "December 14th, 2001"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:19
msgid "missing - missing manual pages"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:30
msgid ""
"This manual page intends to document which manual pages are missing in "
"various sections.  Bug reports against the package B<manpages-dev> or "
"B<manpages> were opened for all of them through the Debian Bug Tracking "
"System (BTS).  Some of these bug reports were reported upstream while some "
"weren't.  Upstream, however, is not able to create arbitrary manpages "
"without additional information and additional spare time.  Hence, it's "
"completely useless to simply report them upstream."
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:36
msgid ""
"Manpages represent an important means of documentation which can easily be "
"read and converted into various formats.  They are very helpful for checking "
"how things work, rather than for a tutorial.  If you notice that more pages "
"are missing, please try to find additional information and report it as "
"wishlist bug against this package."
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:43
msgid ""
"If you are confident with the issue, please write up a preliminary manpage "
"and attach it to the bug report.  It doesn't matter if your English is bad "
"or if you mixed up markup, as long as the content can be used as source for "
"a new manpage.  A detailed documentation on how to write manpages is "
"available in B<man>(7)."
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:48
msgid ""
"When writing manual pages please ensure that they are conforming with The "
"Single UNIX Specification (see below).  Linux ought to be conforming to this "
"specification.  Differences need to be documented, in additional sections, "
"though."
msgstr ""

#. type: SH
#: build/C/man7/missing.7:48
#, no-wrap
msgid "MISSING PAGES"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:56
msgid ""
"The following is a list of missing pages sorted by section and bug number.  "
"If you can contribute content or even a manual page, please send a mail to "
"I<nnnnn>B<@bugs.debian.org> where I<nnnnn> refers to the bug number from "
"below."
msgstr ""

#. type: SS
#: build/C/man7/missing.7:56
#, no-wrap
msgid "Section 2 - System calls"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:57
#, no-wrap
msgid "Bug#I<235963>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:61
msgid "B<timer_create>(2)  - create a per-process timer"
msgstr ""

#. type: SS
#: build/C/man7/missing.7:61
#, no-wrap
msgid "Section 3 - Library calls"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:62
#, no-wrap
msgid "Bug#I<123999>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:66
msgid "B<obstack_alloc>(3)  - allocate memory"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:69
msgid "B<obstack_free>(3)  - free formerly allocated memory"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:72
msgid "B<obstack_copy>(3)  - allocate block with content"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:75
msgid "B<obstack_copy0>(3)  - allocate block with content, zero terminated"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:78
msgid "B<obstack_blank>(3)  - increase object size"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:81
msgid "B<obstack_grow>(3)  - increase object size with content"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:84
msgid "B<obstack_grow0>(3)  - increase object size with content, zero term."
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:87
msgid "B<obstack_1grow>(3)  - increase object size by 1 character"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:90
msgid "B<obstack_ptr_grow>(3)  - increase object size by value of pointer"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:93
msgid "B<obstack_int_grow>(3)  - increase object size by sizeof(int)"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:96
msgid "B<obstack_finish>(3)  - finish growing of an obstack object"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:99
msgid "B<obstack_object_size>(3)  - return the size of an obstack object"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:102
msgid "B<obstack_room>(3)  - available room in current chunk"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:105
msgid "B<obstack_1grow_fast>(3)  - fast increase object size by 1 character"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:108
msgid "B<obstack_ptr_grow_fast>(3)  - fast increase object by pointer value"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:111
msgid "B<obstack_int_grow_fast>(3)  - fast increase object size by sizeof(int)"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:114
msgid "B<obstack_blank_fast>(3)  - fast increase object size"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:117
msgid "B<obstack_base>(3)  - return tentative address of beginning"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:120
msgid "B<obstack_next_free>(3)  - return address of the first free byte"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:123
msgid "B<obstack_object_size>(3)  - return size of currently growing object"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:126
msgid "B<obstack_alignment_mask>(3)  - alter mask assignment"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:129
msgid "B<obstack_chunk_size>(3)  - return chunk size of given obstack"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:132
msgid "B<obstack_init>(3)  - initialize use of an obstack"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:132
#, no-wrap
msgid "B<sigrelse>(3), B<sigignore>(3), B<sigpause>(3)"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:135
msgid "- signal management"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:135
#, no-wrap
msgid "Bug#I<147778>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:139
msgid "B<sigwait>(2)  - wait for queued signals"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:139
#, no-wrap
msgid "Bug#I<160225>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:143
msgid "B<sem_close>(3)  - close a named semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:146
msgid "B<sem_destroy>(3)  - destroy an unnamed semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:149
msgid "B<sem_getvalue>(3)  - get the value of a semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:152
msgid "B<sem_init>(3)  - initialize an unnamed semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:155
msgid "B<sem_open>(3)  - initialize/open a named semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:158
msgid "B<sem_post>(3)  - increment the count of a semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:161
msgid "B<sem_trywait>(3), B<sem_wait>(3)  - acquire or wait for a semaphore"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:164
msgid "B<sem_unlink>(3)  - remove a named semaphore"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:164
#, no-wrap
msgid "Bug#I<172139>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:168
msgid "B<wcstol>(3)  - convert a wide-character string to a long integer"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:168
#, no-wrap
msgid "Bug#I<155334>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:172
msgid "B<pthread_mutex>(3)  - Mutex routines for threads"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:172
#, no-wrap
msgid "Bug#I<182706>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:176
msgid "B<fopencookie>(3)  - open stream for communicating with the cookie"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:176
#, no-wrap
msgid "Bug#I<202022>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:181
msgid ""
"B<rcmd_af>(3), B<rresvport_af>(3), B<iruserok_sa>(3)  - routines for "
"returning a stream to a remote command (see B<rcmd>(3))."
msgstr ""

#. type: TP
#: build/C/man7/missing.7:181
#, no-wrap
msgid "Bug#I<208856>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:185
msgid ""
"B<cap_set_proc>(3), B<cap_get_proc>(3)  - POSIX capability manipulation on "
"processes"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:188
msgid ""
"B<capsetp>(3), B<capgetp>(3)  - Linux specific capability manipulation on "
"arbitrary processes"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:188
#, no-wrap
msgid "Bug#I<235967>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:192
msgid ""
"B<if_nametoindex>(3)  - map network interface name to corresponding index."
msgstr ""

#. type: TP
#: build/C/man7/missing.7:192
#, no-wrap
msgid "Bug#I<268121>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:196
msgid ""
"B<rpmatch>(3)  - determine whether a response is affirmative or negative."
msgstr ""

#. type: TP
#: build/C/man7/missing.7:196
#, no-wrap
msgid "Bug#I<349388>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:200
msgid "B<argp_parse>(3)  - main interface to argp."
msgstr ""

#. type: SS
#: build/C/man7/missing.7:200
#, no-wrap
msgid "Section 4 - Special files"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:201
#, no-wrap
msgid "Bug#I<209323>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:205
msgid "B<pty>(4)  - pseudo terminal driver"
msgstr ""

#. type: SS
#: build/C/man7/missing.7:205
#, no-wrap
msgid "Section 9 - Kernel routines"
msgstr ""

#. type: TP
#: build/C/man7/missing.7:206
#, no-wrap
msgid "Bug#I<102724>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:211
msgid ""
"Mandrake seems to deliver section 9 manual pages, though their source is "
"obscure and they do seem to be out-dated.  Nevertheless, some section 9 "
"manpages would be nice, indeed."
msgstr ""

#. type: TP
#: build/C/man7/missing.7:211
#, no-wrap
msgid "Bug#I<179475>"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:215
msgid "B<bdflush>(9)  - flush buffers to disk"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:218
msgid "B<kapmd>(9)  - kernel APM thread"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:221
msgid "B<keventd>(9)  - manage hotplug events"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:224
msgid "B<khubd>(9)  - kernel USB hub daemon thread"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:227
msgid "B<kjournald>(9)  - maintain the filesystem journal"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:230
msgid "B<ksoftirqd>(9)  - software handling of incoming IRQs"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:233
msgid "B<kswapd>(9)  - kernel swap daemon thread"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:236
msgid "B<kupdated>(9)  - flush the journal"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:239
msgid "B<scsi_eh>(9)  - kernel SCSI error handler thread"
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:243
msgid "B<undocumented>(3), B<undocumented>(7), B<man>(7)."
msgstr ""

#. type: Plain text
#: build/C/man7/missing.7:249
msgid ""
"Debian Bug Tracking System at E<lt>B<http://bugs.debian.org/manpages-"
"dev>E<gt>, the Single UNIX Specification, Version 2, at E<lt>B<http://www."
"opengroup.org/onlinepubs/007908799/toc.htm>E<gt>, the Single UNIX "
"Specification, Version 3, at E<lt>B<http://www.UNIX-systems.org/version3/"
">E<gt>."
msgstr ""

#. type: TH
#: build/C/man7/undocumented.7:27
#, no-wrap
msgid "August 24th, 2003"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:30
msgid "undocumented - No manpage for this program, utility or function."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:40
msgid ""
"This B<program>, B<utility> or B<function> does not have a useful manpage.  "
"Before opening a bug to report this, please check with the Debian Bug "
"Tracking System (BTS) at E<lt>I<http://bugs.debian.org/>E<gt> if a bug has "
"already been reported.  If not, you can submit a wishlist bug if you want."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:50
msgid ""
"If you are a competent and accurate writer and are willing to spend the time "
"reading the source code and writing good manpages please write a better man "
"page than this one.  Please B<contact> the B<package maintainer> and copy "
"I<man-pages@qa.debian.org> in order to avoid several people working on the "
"same manpage."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:56
msgid ""
"Even if you are not an accurate writer, your input may be helpful.  Writing "
"manual pages is quite easy, the format is described in B<man>(7).  The most "
"important and time-consuming task is to collect the information to be put in "
"the new manpage."
msgstr ""

#. type: SH
#: build/C/man7/undocumented.7:57
#, no-wrap
msgid "DIAGNOSTICS"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:63
msgid ""
"It is possible that the man page for the command you specified is installed "
"and that your manual page index caches are out of sync. You should try "
"running B<mandb>(8)."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:65
msgid "Try the following options if you want more information:"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:71
msgid "I<foo> B<--help>, I<foo> B<-h>, I<foo>B< -?>"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:73
msgid "B<info>I< foo>"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:77
msgid "B<whatis> B<foo>, B<apropos>I< foo>"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:81
msgid "B<dpkg --listfiles> I<foo>, B<dpkg --search >I<foo>"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:83
msgid "B<locate '*>I<foo>B<*'>"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:85
msgid "B<find / -name '*>I<foo>B<*'>"
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:89
msgid ""
"Additionally, check the directories I</usr/share/doc/foo>, I</usr/lib/foo>."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:92
msgid ""
"The documentation might be in a package starting with the same name as the "
"package the software belongs to, but ending with -doc or -docs."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:96
msgid ""
"If you still didn't find the information you are looking for you might "
"consider posting a call for help to I<debian-user@lists.debian.org>."
msgstr ""

#. type: Plain text
#: build/C/man7/undocumented.7:107
msgid ""
"B<info>(1), B<whatis>(1), B<apropos>(1), B<dpkg>(8), B<locate>(1), B<find>"
"(1), B<updatedb>(1), B<undocumented>(2), B<man>(7), B<mandb>(8), B<missing>"
"(7)."
msgstr ""

#. type: Plain text
#: build/C/man2/getgid.2:48
msgid ""
"When a normal program is executed, the effective and real group ID of the "
"process are set to the group ID of the user executing the file.  When a set "
"ID program is executed the real group ID is set to the group of the calling "
"user and the effective user ID corresponds to the set group ID bit on the "
"file being executed."
msgstr ""

#. type: Plain text
#: build/C/man2/getuid.2:49
msgid ""
"When a normal program is executed, the effective and real user ID of the "
"process are set to the ID of the user executing the file.  When a set ID "
"program is executed the real user ID is set to the calling user and the "
"effective user ID corresponds to the set ID bit on the file being executed."
msgstr ""

#. type: Plain text
#: build/C/man2/socket.2:388
msgid ""
"The header file I<E<lt>sys/types.hE<gt>> is only required for libc4 or "
"earlier.  Some packages, like util-linux, claim portability to all Linux "
"versions and libraries.  They certainly need this header file."
msgstr ""

#. type: Plain text
#: build/C/man3/bsearch.3:87
#, no-wrap
msgid "#define nr_of_months (sizeof(months)/sizeof(struct mi))\n"
msgstr ""

#. type: Plain text
#: build/C/man3/dbopen.3:44
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>limits.hE<gt>>\n"
"B<#include E<lt>db.hE<gt>>\n"
"B<#include E<lt>fcntl.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: build/C/man3/fclose.3:58
msgid ""
"The B<fclose>()  function flushes the stream pointed to by I<fp> (writing "
"any buffered output data using B<fflush>(3))  and closes the underlying file "
"descriptor."
msgstr ""

#. type: Plain text
#: build/C/man3/fclose.3:66
msgid ""
"The behaviour of B<fclose>()  is undefined if the I<stream> parameter is an "
"illegal pointer, or is a descriptor already passed to a previous invocation "
"of B<fclose>()."
msgstr ""

#. type: Plain text
#: build/C/man3/fclose.3:77
msgid ""
"Upon successful completion 0 is returned.  Otherwise, B<EOF> is returned and "
"the global variable I<errno> is set to indicate the error.  In either case "
"any further access (including another call to B<fclose>())  to the stream "
"results in undefined behavior."
msgstr ""

#. type: Plain text
#: build/C/man3/getpwent.3:104
msgid ""
"When B<shadow>(5)  passwords are enabled (which is default on many GNU/Linux "
"installations) the content of I<pw_passwd> is usually not very useful.  In "
"such a case most passwords are stored in a separate file."
msgstr ""

#. type: Plain text
#: build/C/man3/getpwent.3:110
msgid ""
"The variable I<pw_shell> may be empty, in which case the system will execute "
"the default shell (B</bin/sh>)  for the user."
msgstr ""

#. type: Plain text
#: build/C/man3/getpwent.3:123
msgid ""
"The B<getpwent>()  function returns a pointer to a I<passwd> structure, or "
"NULL if there are no more entries or an error occured.  If an error occurs, "
"I<errno> is set appropriately.  If one wants to check I<errno> after the "
"call, it should be set to zero before the call."
msgstr ""

#.  Next line rejected upstream
#. type: Plain text
#: build/C/man3/getpwent.3:173
msgid ""
"B<fgetpwent>(3), B<getpw>(3), B<getpwent_r>(3), B<getpwnam>(3), B<getpwuid>"
"(3), B<putpwent>(3), B<shadow>(5), B<passwd>(5)"
msgstr ""

#. type: Plain text
#: build/C/man3/getpwnam.3:201
msgid ""
"The user password database mostly refers to I</etc/passwd>.  However, with "
"recent systems it also refers to network wide databases using NIS, LDAP and "
"other local files as configured in I</etc/nsswitch.conf>."
msgstr ""

#. type: Plain text
#: build/C/man3/getpwnam.3:208
msgid "System Databases and Name Service Switch configuration file"
msgstr ""

#. type: Plain text
#: build/C/man3/getpwnam.3:312
msgid ""
"B<endpwent>(3), B<fgetpwent>(3), B<getgrnam>(3), B<getpw>(3), B<getpwent>"
"(3), B<getspnam>(3), B<putpwent>(3), B<setpwent>(3), B<nsswitch.conf>(5), "
"B<passwd>(5)"
msgstr ""

#. type: Plain text
#: build/C/man3/nl_langinfo.3:34
msgid ""
"The B<nl_langinfo>()  function provides access to locale information in a "
"more flexible way than B<localeconv>(3)  does.  Individual and additional "
"elements of the locale categories can be queried.  B<setlocale>(3)  needs to "
"be executed with proper arguments before."
msgstr ""

#. type: Plain text
#: build/C/man3/qsort.3:110
#, no-wrap
msgid "    qsort(&argv[1], argc - 1, sizeof(argv[1]), cmpstringp);\n"
msgstr ""

#. type: Plain text
#: build/C/man3/setnetgrent.3:81
msgid ""
"In most cases you only want to check if the triplet B<(hostname>,B<username>,"
"B<domainname)> is a member of a netgroup.  The function B<innetgr>()  can be "
"used for this without calling the above three functions.  Again, a NULL "
"pointer is a wildcard and matches any string.  The function is thread-safe."
msgstr ""

#. type: Plain text
#: build/C/man3/system.3:155
msgid ""
"If the B<_XOPEN_SOURCE> feature test macro is defined, then the macros "
"described in B<wait>(2)  (B<WEXITSTATUS>(), etc.) are made available when "
"including E<lt>stdlib.hE<gt>."
msgstr ""

#. type: Plain text
#: build/C/man3/tsearch.3:117
msgid ""
"B<twalk>()  performs depth-first, left-to-right traversal of a binary tree.  "
"I<root> points to the starting node for the traversal.  If that node is not "
"the root, then only part of the tree will be visited.  B<twalk>()  calls the "
"user function I<action> each time a node is visited (that is, three times "
"for an internal node, and once for a leaf).  I<action>, in turn, takes three "
"arguments.  The first is a pointer to the node being visited.  The second is "
"an integer which takes on the values B<preorder>, B<postorder>, and "
"B<endorder> depending on whether this is the first, second, or third visit "
"to the internal node, or B<leaf> if it is the single visit to a leaf node.  "
"(These symbols are defined in I<E<lt>search.hE<gt>>.)  The third argument is "
"the depth of the node, with zero being the root.  You should not modify the "
"tree while traversing it as the the results would be undefined."
msgstr ""

#.  .BR wscanf (3)
#. type: Plain text
#: build/C/man3/wprintf.3:228
msgid "B<fprintf>(3), B<fputwc>(3), B<fwide>(3), B<printf>(3), B<snprintf>(3)."
msgstr ""

#. type: tbl table
#: build/C/man4/console_codes.4:549 build/C/man4/console_codes.4:551
#: build/C/man4/console_codes.4:553
#, no-wrap
msgid "\t\tHas no visible effect in xterm.\n"
msgstr ""

#.  Rejected upstream
#. type: Plain text
#: build/C/man5/dir_colors.5:367
msgid ""
"(Slackware, SuSE and RedHat only; ignored by GNU B<dircolors>(1)  and thus "
"Debian.)  System-wide configuration file."
msgstr ""

#.  Rejected upstream
#. type: Plain text
#: build/C/man5/dir_colors.5:374
msgid ""
"(Slackware, SuSE and RedHat only; ignored by GNU B<dircolors>(1)  and thus "
"Debian.)  Per-user configuration file."
msgstr ""

#. type: Plain text
#: build/C/man5/motd.5:43
msgid ""
"On Debian GNU/Linux this file is a symbolic link pointing to I</var/run>.  "
"The contents of this file are regenerated upon every system boot based on "
"the contents of I</etc/motd.tail>."
msgstr ""

#. type: Plain text
#: build/C/man5/motd.5:51
msgid "B<login>(1), B<motd.tail>(5), B<issue>(5)"
msgstr ""

#. type: Plain text
#: build/C/man5/networks.5:41
msgid ""
"where the fields are delimited by spaces or tabs.  Empty lines are ignored.  "
"If a line contains a hash mark (#), the hash mark and the remaining part of "
"the line are ignored."
msgstr ""

#. type: Plain text
#: build/C/man5/networks.5:47
msgid "The symbolic name for the network."
msgstr ""

#. type: Plain text
#: build/C/man5/networks.5:51
msgid ""
"The official number for this network in dotted-decimal notation.  The "
"trailing \".0\" may be omitted."
msgstr ""

#. type: Plain text
#: build/C/man5/networks.5:63
msgid ""
"This file is read by B<route> or B<netstat> utilities.  Only Class A, B or C "
"networks are supported, partitioned networks (i.e. network/26 or network/28) "
"are not supported by this facility."
msgstr ""

#. type: Plain text
#: build/C/man5/nsswitch.conf.5:228
msgid ""
"Linux libc5 without NYS does not have the name service switch but does allow "
"the user some policy control.  In I</etc/passwd> you could have entries of "
"the form +user or +@netgroup (include the specified user from the NIS passwd "
"map), -user or -@netgroup (exclude the specified user), and + (include every "
"user, except the excluded ones, from the NIS passwd map)."
msgstr ""

#. type: Plain text
#: build/C/man5/nsswitch.conf.5:233
msgid ""
"You can override certain passwd fields for a particular user from the NIS "
"passwd map by using the extended form of +user:::::: in I</etc/passwd>.  Non-"
"empty fields override information in the NIS passwd map."
msgstr ""

#. type: Plain text
#: build/C/man5/nsswitch.conf.5:252
msgid ""
"Since most people only put a + at the end of I</etc/passwd> to include "
"everything from NIS, the switch provides a faster alternative for this case "
"(`passwd: files nis') which doesn't require the single + entry in I</etc/"
"passwd>, I</etc/group>, and I</etc/shadow>.  If this is not sufficient, the "
"NSS `compat' service provides full +/- semantics.  By default, the source is "
"`nis', but this may be overridden by specifying `nisplus' as source for the "
"pseudo-databases B<passwd_compat>, B<group_compat> and B<shadow_compat>.  "
"These pseudo-databases are only available in GNU C Library."
msgstr ""

#.  Debian-only note?
#. type: Plain text
#: build/C/man5/nsswitch.conf.5:300
msgid ""
"On a Debian system other mail transport agents may or may not ignore the "
"I<aliases> file.  For example, unlike B<sendmail> Exim does not ignore it."
msgstr ""

#. type: Plain text
#: build/C/man5/resolv.conf.5:41
msgid ""
"If this file doesn't exist the only name server to be queried will be on the "
"local machine; the domain name is determined from the hostname and the "
"domain search path is constructed from the domain name."
msgstr ""

#. type: Plain text
#: build/C/man5/resolv.conf.5:203
msgid "Some programs behave strangely when this option is turned on."
msgstr ""

#. type: Plain text
#: build/C/man5/tzfile.5:7
msgid "tzfile - time zone information"
msgstr ""

#. type: Plain text
#: build/C/man5/tzfile.5:13
msgid ""
"This page describes the structure of timezone files as commonly found in I</"
"usr/lib/zoneinfo> or I</usr/share/zoneinfo>."
msgstr ""

#. type: Plain text
#: build/C/man5/tzfile.5:25
msgid ""
"The time zone information files used by B<tzset>(3)  begin with the magic "
"characters \"TZif\" to identify then as time zone information files, "
"followed by sixteen bytes reserved for future use, followed by six four-byte "
"values of type I<long>, written in a \"standard\" byte order (the high-order "
"byte of the value is written first).  These values are, in order:"
msgstr ""

#. type: Plain text
#: build/C/man5/tzfile.5:144
msgid ""
"This manual page documents I<E<lt>tzfile.hE<gt>> in the glibc source "
"archive, see I<timezone/tzfile.h>."
msgstr ""

#. type: Plain text
#: build/C/man5/tzfile.5:152
msgid ""
"It seems that timezone uses B<tzfile> internally, but glibc refuses to "
"expose it to userspace.  This is most likely because the standardised "
"functions are more useful and portable, and actually documented by glibc.  "
"It may only be in glibc just to support the non-glibc-maintained timezone "
"data (which is maintained by some other entity)."
msgstr ""

#.  .BR newctime (3)
#. type: Plain text
#: build/C/man5/tzfile.5:158
msgid "B<time>(3), B<gettimeofday>(3), B<tzset>(3), B<ctime>(3)"
msgstr ""

#. type: Plain text
#: build/C/man7/man.7:562
msgid ""
"B<apropos>(1), B<groff>(1), B<man>(1), B<man2html>(1), B<groff_mdoc>(7), "
"B<whatis>(1), B<groff_man>(7), B<groff_www>(7), B<man-pages>(7), B<mdoc>(7)"
msgstr ""

#. type: Plain text
#: build/C/man7/mdoc.7:64
msgid ""
"The E<.Nm \\-mdoc> package is a set of content-based and domain-based macros "
"used to format the E<.Bx> man pages.  The macro names and their meanings are "
"listed below for quick reference; for a detailed explanation on using the "
"package, see the tutorial sampler E<.Xr groff_mdoc 7>."
msgstr ""
