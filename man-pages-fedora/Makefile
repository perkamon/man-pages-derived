
SUBDIRS := man-fedora

PO4AFLAGS = -k 100 --msgmerge-opt '-C po4a-local/po/compendium-$$lang.po'

TOPDIR = ../../man-pages

all: setup

include $(TOPDIR)/Makefile.common

# Fedora package number
P = 6.fc13

#  Download tarball
get-orig-source: man-pages-$(V)-$(P).noarch.rpm
man-pages-$(V)-$(P).noarch.rpm:
	wget http://kojipkgs.fedoraproject.org/packages/man-pages/$(V)/$(P)/noarch/$@

#  Unpack sources
unpack: stamp-unpack
stamp-unpack: man-pages-$(V)-$(P).noarch.rpm
	-rm -rf man-pages usr
	rpm2cpio $? | cpio -i -d
	mv usr/share/man man-pages
	-rm -rf usr
	gunzip man-pages/man*/*.gz man-pages/en/man*/*.gz
	touch $@

#  Prepare sources to be used by po4a
#  This target is called once after manual pages are unpacked.
setup: stamp-setup
stamp-setup: stamp-unpack
	-rm -rf build link
	mkdir -p build/C
	for i in $$(seq 8); do mkdir build/C/man$$i; done
	#  Some manual pages are only aliases, they contain a single line
	#      .so target_man_page
	#  Create a file named 'link', its format is:
	#      target_man_page src_man_page
	#  This file is used after pages are translated to create aliases
	#  of translated manual pages.
	set -e; cd man-pages; for f in man?/*.?; do \
	  if sed -e '1!d' $$f | grep -q '^\.so'; \
	  then \
	    sed -e '1!d' -e 's/^\.so //' -e "s,$$, $$f," $$f >> ../link; \
	  elif test -e en/$$f; \
	  then \
	    sed -e 's/$$//' en/$$f | iconv -f utf8 -t latin1 > ../build/C/$$f; \
	  else \
	    sed -e 's/$$//' $$f > ../build/C/$$f; \
	  fi; \
	done
	LC_ALL=C sort link > temp && mv temp link
	#  Remove empty directories, if any
	-rmdir build/C/man* 2>/dev/null
	#  armscii-8 encoding is missing in Perl, convert to UTF-8 to make po4a work
	iconv -f armscii-8 -t UTF-8 build/C/man7/armscii-8.7 | sed -e '1s/coding: ARMSCII-8/coding: UTF-8/' > temp && mv temp build/C/man7/armscii-8.7
	#   Remove files which are modified in Fedora and cannot be processed by po4a
	rm -f build/C/man2/clock_getres.2 build/C/man2/clock_nanosleep.2 build/C/man2/delete_module.2 build/C/man2/get_mempolicy.2 build/C/man2/init_module.2 build/C/man2/set_tid_address.2 build/C/man2/timer_create.2 build/C/man2/timer_delete.2 build/C/man2/timer_settime.2 build/C/man2/timer_getoverrun.2
	#   These files are UTF-8 encoded but have wrong coding specified.
	#   Recode them in original encoding as specified in man*.cfg
	for f in build/C/man7/iso*.7 build/C/man7/koi8-r.7; do \
	  echo $$f; \
	  iconv -f UTF-8 -t $$(echo $$f | sed -e 's,.*/,,' -e 's,\.7$$,,' -e 's/_//') $$f > temp && mv temp $$f; \
	done
	#  Apply patches to fix groff syntax errors which  prevent po4a processing
	for p in $(CURDIR)/fedora-fixes.patch $(CURDIR)/$(TOPDIR)/po4a-fixes.patch; do \
	  if test -f $$p; \
	  then \
	    cd build/C && patch -p1 < $$p; \
	    break; \
	  fi; \
	done
	# Copy po4a files from man-pages
	-rm -rf po4a
	tar cf - --exclude=.svn -C $(TOPDIR) po4a | tar xf -
	# Compendium and .pot files for original strings
	msgcat --use-first po4a/man*/po/man*.pot > po4a-local/po/man-pages.pot
	langs='$(LANGS)'; for l in $$langs; do \
	  msgcat --use-first po4a/man*/po/$$l.po > po4a-local/po/compendium-$$l.po; \
	done
	# Files added in Fedora
	mkdir -p po4a/man-fedora/po
	cp po4a-local/man-fedora.cfg po4a/man-fedora/man-fedora.cfg
	-cp po4a-local/po/*.po po4a/man-fedora/po/
	tar cf - --exclude=.svn -C po4a-local add_fr | tar xf - -C po4a
	# Files removed in Fedora
	$(MAKE) disable-removed
	# Generate new .pot files for modified strings
	for f in po4a/man*/man*.cfg; do po4a --force --no-translations $$f; done
	# Extract specific strings
	$(MAKE) po4a-local/po/fedora.pot updatepo
	touch $@

stats:: $(patsubst %, local-stats-%, $(LANGS))
local-stats-%:
	@LC_ALL=C msgfmt -c --statistics -o /dev/null po4a-local/po/$*.po

po4a-local/po/fedora.pot: FORCE
	@test -d po4a-local/po || mkdir po4a-local/po
	# Do not merge man-fedora and man[0-9]* into a single glob!
	# POT and PO files would depend upon locale used when building
	msgcat --use-first po4a/man-fedora/po/man*.pot po4a/man[0-9]*/po/man*.pot > man-pages-dist-fedora.pot
	cat po4a-local/po/man-pages.pot | msgcat --less-than=2 po4a-local/po/man-pages.pot - man-pages-dist-fedora.pot > temp && mv temp $@

updatepo:: po4a-local/po/fedora.pot $(patsubst %, updatepo-%, $(LANGS))

updatepo-%: po4a-local/po/fedora.pot FORCE
	msgmerge --previous po4a-local/po/$*.po po4a-local/po/fedora.pot  > po4a-local/po/new$*.po
	msgattrib --no-obsolete po4a-local/po/new$*.po > po4a-local/po/$*.po
	# No fuzzy matching, otherwise string remains fuzzy even if there is a right entry in compendium
	set -e; for f in po4a/man*/po/$*.po; do \
	  msgcat --use-first po4a-local/po/$*.po $$f | msgattrib --no-obsolete --no-fuzzy - > temp && mv temp $$f; \
	done

clean::
	-rm -f stamp-* link temp
	-rm -rf man-pages build
	rm -f *.pot
	rm -f po4a-local/po/new*.po po4a-local/po/compendium* po4a-local/po/man-pages.pot
	rm -rf po4a

.PHONY: FORCE

